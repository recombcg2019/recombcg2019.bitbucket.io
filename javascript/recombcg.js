  //These are functions for highlighting navbar sections on the screen.

/*
 * Return True if the function supports IntersectionObserver.
 */
function supportsIntersectionObserver()
  {
  return ('IntersectionObserver' in window &&
          'IntersectionObserverEntry' in window)
  }

if(supportsIntersectionObserver())
  {   //For modern browsers (and not cutting edge Safari):
  const sections = ['home', 'city', 'venue', 'keydates', 'organization', 'contact'];
  
  function highlightCheck(entries)
    {
    for (var e of entries)
      {
      var navitem =  document.querySelector('#' + e.target.id + 'nav')
      if(e.isIntersecting) // &&
         //(e.intersectionRect.height/e.target.offsetHeight > 0.5 ||
         // e.intersectionRect.height/e.rootBounds.height > 0.5))
        {
        navitem.classList.add('active')
        //if(e.target.id == 'home')
        //  document.getElementById('navimage').src = "images/rcgicon_white.svg";
        }
      else
        {
        navitem.classList.remove('active')
        //if(e.target.id == 'home')
        //  document.getElementById('navimage').src = "images/rcgicon.svg";
        }
      }
    }
  
  
  var io = new IntersectionObserver(
    highlightCheck,      //Call this function when crossing this much:
    { });
    //{ threshold: [0, 0.25, 0.75, 1] });
  
  for (var s of sections)
    { io.observe(document.querySelector('#'+s)) }
  }
else
  { //For older browsers (just highlight the clicked entry):
  window.onload = function()
    {
    const x = document.getElementsByClassName("navitem");
    var i;
    for (i = 0; i < x.length; i++)
      { x[i].addEventListener('click', highlight); }
    }
  }


// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
    //These are functions for changing the navbar when it sticks:

/*
 * If the header image is not visible, then the topnav bar is stuck.
 * In which case, change the navbar icon.
 */
function stickyCheck(entries)
  {
  //topnav = document.querySelector('.topnav')
  var ni = document.getElementById('navimage');
  if(!entries[0].isIntersecting)    //stuck
    {
    //document.getElementById('navimage').src = "images/rcgicon.svg";
    ni.classList.add('stuck');
    ni.classList.remove('unstuck');
    }
  else                              //unstuck
    {
    //document.getElementById('navimage').src = "images/rcgicon_empty.svg";
    if(ni.classList.contains('stuck'))
      {
      ni.classList.add('unstuck');
      ni.classList.remove('stuck');
      }
    }
  }

var stickyio = new IntersectionObserver(stickyCheck, {});
stickyio.observe(document.querySelector('header'))


// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
//  These are function for highlighting the header that is clicked:

function unhighlightAll(event)
  {
  const x = document.getElementsByClassName("navitem");
  var i;
  for (i = 0; i < x.length; i++)
    { x[i].classList.remove('active'); }
  document.getElementById('navimage').src = "images/rcgicon.svg";
  }

function highlight(event)
  {
  unhighlightAll()
  if(event.target.id == 'navimage')
    {
    event.target.parentNode.classList.add('active');
    event.target.src = "images/rcgicon_white.svg";
    }
  else
    { event.target.classList.add('active'); }
  }
