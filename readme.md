#Editing the RECOMB-CG Site
To add a section to the HTML file you need to do a few things:

1. Add the section using the `<section id='SECTION_NAME'></section>` tags, giving an appropriate section name in place of `SECTION_NAME`. 
2. Add the navbar menu item to the `topnav` list (with copy/paste) while setting the id to `SECTION_NAMEnav` (append "nav" to the section name).
3. In `javascript/recombcg.js`, add the `SECTION_NAME` to the array `sections`.


NOTE: if we run out of space for navigation bar header, then we can use [dropdown menus](https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar) dropdown menus.
